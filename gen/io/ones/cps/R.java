/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package io.ones.cps;

public final class R {
    public static final class array {
        public static final int pref_example_list_titles=0x7f080000;
        public static final int pref_example_list_values=0x7f080001;
        public static final int pref_sync_frequency_titles=0x7f080002;
        public static final int pref_sync_frequency_values=0x7f080003;
    }
    public static final class attr {
    }
    public static final class color {
        public static final int ActionBar=0x7f050010;
        public static final int aqua=0x7f050009;
        public static final int black=0x7f05000f;
        public static final int blue=0x7f05000d;
        public static final int fuchsia=0x7f050002;
        public static final int gray=0x7f050005;
        public static final int green=0x7f05000c;
        public static final int lime=0x7f05000a;
        public static final int maroon=0x7f050008;
        public static final int navy=0x7f05000e;
        public static final int olive=0x7f050006;
        public static final int purple=0x7f050007;
        public static final int red=0x7f050003;
        public static final int silver=0x7f050004;
        public static final int teal=0x7f05000b;
        public static final int white=0x7f050000;
        public static final int yellow=0x7f050001;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Example customization of dimensions originally defined in res/values/dimens.xml
         (such as screen margins) for screens with more than 820dp of available width. This
         would include 7" and 10" devices in landscape (~960dp and ~1280dp respectively).
    
         */
        public static final int activity_horizontal_margin=0x7f060000;
        public static final int activity_vertical_margin=0x7f060001;
        /** 
         Per the design guidelines, navigation drawers should be between 240dp and 320dp:
         https://developer.android.com/design/patterns/navigation-drawer.html
    
         */
        public static final int navigation_drawer_width=0x7f060002;
    }
    public static final class drawable {
        public static final int drawer_shadow=0x7f020000;
        public static final int ic_action=0x7f020001;
        public static final int ic_action_bar=0x7f020002;
        public static final int ic_action_camera=0x7f020003;
        public static final int ic_action_name=0x7f020004;
        public static final int ic_drawer=0x7f020005;
        public static final int ic_launcher=0x7f020006;
        public static final int loading=0x7f020007;
        public static final int login=0x7f020008;
        public static final int party=0x7f020009;
        public static final int white=0x7f02000a;
    }
    public static final class id {
        public static final int FrameLayout1=0x7f0b0000;
        public static final int action_settings=0x7f0b0018;
        public static final int big_img=0x7f0b0017;
        public static final int container=0x7f0b0008;
        public static final int drawer_layout=0x7f0b0007;
        public static final int home_list=0x7f0b000b;
        public static final int loginButton=0x7f0b0006;
        public static final int navigation_drawer=0x7f0b0009;
        public static final int new_img=0x7f0b000c;
        public static final int passwordEditText=0x7f0b0005;
        public static final int passwordTextView=0x7f0b0004;
        public static final int profile_gridview=0x7f0b000f;
        public static final int profile_img=0x7f0b000d;
        public static final int profile_thumb=0x7f0b0015;
        public static final int profile_username=0x7f0b000e;
        public static final int small_img=0x7f0b000a;
        public static final int story_img=0x7f0b0010;
        public static final int take_picture=0x7f0b0019;
        public static final int turn_down_count=0x7f0b0013;
        public static final int turn_down_symbol=0x7f0b0012;
        public static final int turn_up_count=0x7f0b0011;
        public static final int turn_up_symbol=0x7f0b0014;
        public static final int userNameEditText=0x7f0b0003;
        public static final int username=0x7f0b0016;
        public static final int usernameTextView=0x7f0b0002;
        public static final int welcomeTextView=0x7f0b0001;
    }
    public static final class layout {
        public static final int activity_login=0x7f030000;
        public static final int activity_main=0x7f030001;
        public static final int activity_new_story=0x7f030002;
        public static final int activity_story=0x7f030003;
        public static final int cell=0x7f030004;
        public static final int fragment_home=0x7f030005;
        public static final int fragment_main=0x7f030006;
        public static final int fragment_navigation_drawer=0x7f030007;
        public static final int fragment_new_story=0x7f030008;
        public static final int fragment_profile=0x7f030009;
        public static final int fragment_story=0x7f03000a;
        public static final int navigation_listview_layout=0x7f03000b;
        public static final int row=0x7f03000c;
    }
    public static final class menu {
        public static final int global=0x7f0a0000;
        public static final int login=0x7f0a0001;
        public static final int main=0x7f0a0002;
        public static final int new_story=0x7f0a0003;
        public static final int profile=0x7f0a0004;
        public static final int story=0x7f0a0005;
    }
    public static final class string {
        public static final int action_example=0x7f070006;
        public static final int action_new_picture=0x7f070012;
        public static final int action_settings=0x7f070007;
        public static final int app_name=0x7f070000;
        public static final int hello_world=0x7f07000a;
        public static final int login=0x7f07000d;
        public static final int navigation_drawer_close=0x7f070005;
        public static final int navigation_drawer_open=0x7f070004;
        public static final int password=0x7f07000c;
        public static final int password_hint=0x7f07000f;
        public static final int pref_default_display_name=0x7f070019;
        public static final int pref_description_social_recommendations=0x7f070017;
        /**  Example settings for Data & Sync 
         */
        public static final int pref_header_data_sync=0x7f07001b;
        /**  Strings related to Settings 
 Example General settings 
         */
        public static final int pref_header_general=0x7f070015;
        /**  Example settings for Notifications 
         */
        public static final int pref_header_notifications=0x7f07001e;
        public static final int pref_ringtone_silent=0x7f070021;
        public static final int pref_title_add_friends_to_messages=0x7f07001a;
        public static final int pref_title_display_name=0x7f070018;
        public static final int pref_title_new_message_notifications=0x7f07001f;
        public static final int pref_title_ringtone=0x7f070020;
        public static final int pref_title_social_recommendations=0x7f070016;
        public static final int pref_title_sync_frequency=0x7f07001c;
        public static final int pref_title_system_sync_settings=0x7f07001d;
        public static final int pref_title_vibrate=0x7f070022;
        public static final int title_activity_login=0x7f070009;
        public static final int title_activity_new_story=0x7f070013;
        public static final int title_activity_profile=0x7f070011;
        public static final int title_activity_settings=0x7f070014;
        public static final int title_activity_story=0x7f070010;
        public static final int title_section1=0x7f070001;
        public static final int title_section2=0x7f070002;
        public static final int title_section3=0x7f070003;
        public static final int username=0x7f07000b;
        public static final int username_hint=0x7f07000e;
        public static final int welcome=0x7f070008;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f090000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f090001;
        public static final int CustomActionBarTheme=0x7f090002;
        /**  ActionBar styles 
         */
        public static final int MyActionBar=0x7f090003;
    }
    public static final class xml {
        public static final int pref_data_sync=0x7f040000;
        public static final int pref_general=0x7f040001;
        public static final int pref_headers=0x7f040002;
        public static final int pref_notification=0x7f040003;
    }
}
