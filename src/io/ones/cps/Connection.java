package io.ones.cps;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.os.AsyncTask;

public class Connection extends AsyncTask<String,Void,String> {

	private String response,body;
	private HttpURLConnection link;

	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		try {
			
			URL url= new URL(params[0]);
			link =(HttpURLConnection) url.openConnection();
			 setResponse(link.getResponseMessage());
			 link.setReadTimeout(400);
			 InputStream in = new BufferedInputStream(link.getInputStream());
			 readStream(in);
			 return body;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		}
	
	  protected void readStream(InputStream in)
	  {
		  BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		  StringBuilder sb = new StringBuilder();
		  String line=null;
		  try {
			while ((line=reader.readLine())!= null)
			  {
				  sb.append(line);
			  }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  body=sb.toString();
	  }
	  public String getBody()
	  {
		  return body;
	  }
	   protected void onPostExecute(Boolean result) {

	     }

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

}
