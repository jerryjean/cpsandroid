package io.ones.cps;

import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {
	
	private Button login;
	private EditText user, pass;
	private final String server_address="http://default-environment-fjb3g7bc6t.elasticbeanstalk.com/login";
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		login = (Button) findViewById(R.id.loginButton);
		user= (EditText) findViewById(R.id.userNameEditText);
		pass= (EditText) findViewById(R.id.passwordEditText);
		final Connection con = new Connection ();

		login.setOnClickListener(new View.OnClickListener() {
		
			@Override
			public void onClick(View v) { //Consider creating a separate method to handle below
				// TODO Auto-generated method stub
				if(user.getText().length()>5 && pass.getText().length()>5)
				{
					try{
						con.execute(server_address);
					} catch(IllegalStateException e)
					{
					System.out.println(e.getMessage());
					}
					try {
						if(con.get() != null)
						{
							Toast.makeText(getApplicationContext(),con.getBody(),Toast.LENGTH_SHORT).show();
							//Intent intent=new Intent(LoginActivity.this, MainActivity.class);
							//startActivity(intent);
						}
					} catch (InterruptedException | ExecutionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
				{
					Toast.makeText(getApplicationContext(),"Please enter valid username and password",Toast.LENGTH_SHORT).show();
				}

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
