package io.ones.cps;
import com.google.gson.annotations.SerializedName;

public class Feed {

@SerializedName("top")
 private String top;

@SerializedName("stories")
private Story[] stories;

public String getTop()
{
	return this.top;
}

public Story[] getStories()
{
	return this.stories;
}

}
