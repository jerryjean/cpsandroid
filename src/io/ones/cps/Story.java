package io.ones.cps;

import com.google.gson.annotations.SerializedName;

public class Story{
	@SerializedName("story_id")
	private String story_id;
	
	@SerializedName("user_id")
	private String user_id;
	
	@SerializedName("username")
	private String username;
	
	@SerializedName("date_posted")
	private String date_posted;
	
	@SerializedName("turn_ups")
	private String turn_ups;
	
	@SerializedName("turn_downs")
	private String turn_downs;
	
	@SerializedName("image_id")
	private String image_id;
	
	@SerializedName("md5")
	private String md5;
	
	@SerializedName("image_permission")
	private String image_permission;
	
	@SerializedName("url")
	private String url;
	
	public int getStoryId()
	{
		return Integer.parseInt(this.story_id);
	}
	public String getURL()
	{
		return this.url;
	}
	public String getMd5()
	{
		return this.md5;
	}
	public String getImageId()
	{
		return image_id;
	}
	public long getDatePosted()
	{
		return Long.parseLong(date_posted);
	}
	
	public String getUserName()
	{
		return username;
	}
}
