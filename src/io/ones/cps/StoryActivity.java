package io.ones.cps;

import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.os.Build;

public class StoryActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_story);
		/*Intent in = new Intent();
		Bundle params=in.getExtras();
		String image_url=null;*/
		String image_url = getIntent().getExtras().getString("io.ones.cps.IMAGE_URL");
		/*if(params!=null)
		{
			image_url=(String) params.get("IMAGE_URL");
			Log.v("StoryActivity",image_url);
		}*/
		if (savedInstanceState == null) {
			
			getFragmentManager().beginTransaction()
					.add(R.id.container, new StoryFragment(image_url)).commit();
		}
		

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.story, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class StoryFragment extends Fragment {
		String image_url;
		public StoryFragment(String url) {
				image_url=url;
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_story,
					container, false);
			if(image_url!=null)
			{
				ImageView img = (ImageView) rootView.findViewById(R.id.story_img);
				img.setTag(image_url);
				Picasso.with(getActivity()).load(image_url).into(img);
				//new LoadImage().execute(img,null);
			}
			return rootView;
		}
	}
}
