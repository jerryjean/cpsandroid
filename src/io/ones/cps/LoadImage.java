package io.ones.cps;


import java.io.InputStream;
import java.net.URL;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.widget.ImageView;

public class LoadImage extends AsyncTask<Object,Void,Drawable>{

	private ImageView view;
	private String url;
	private LruCache<String,Bitmap> cache;

	@Override
	protected Drawable doInBackground(Object... params) {
		// TODO Auto-generated method stub
		view= ((ImageView) params[0]);
		url = view.getTag().toString();
		if(params[1]!=null)
			cache =(LruCache<String, Bitmap>)params[1];
		try {
	        InputStream is = (InputStream) new URL(url).getContent();
	        Drawable d = Drawable.createFromStream(is,"src name");
	        return d;
	    }catch (Exception e) {
	        return null;
	    }
	}
	
	@Override
	protected void onPostExecute(Drawable d)
	{
		if(view.getTag().equals(url))
		{	if(cache!=null)
				cache.put(url,(((BitmapDrawable) d).getBitmap()));
			view.setImageDrawable(d);
		}
	}
	
}