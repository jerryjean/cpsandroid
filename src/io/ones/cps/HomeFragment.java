package io.ones.cps;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass. Activities that contain this fragment
 * must implement the {@link HomeFragment.OnFragmentInteractionListener}
 * interface to handle interaction events. Use the
 * {@link HomeFragment#newInstance} factory method to create an instance of this
 * fragment.
 *
 */

public class HomeFragment extends Fragment implements OnLoadDataListener {
	// TODO: Rename parameter arguments, choose names that match0.
 	// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
	private static final String ARG_PARAM1 = "param1";
	private static final String ARG_PARAM2 = "param2";
	public HomeAdapter adapter;
	public final String TAG= HomeFragment.class.getSimpleName();
	private Story[] stories_cache;
	private String top="";
	private static LruCache<String,Bitmap> lru_cache;
	// TODO: Rename and change types of parameters
	private String mParam1;
	private String mParam2;
	private Drawable D;
	private OnFragmentInteractionListener mListener;
	JsonElement root ; //placeholder for testing gson
	/**
	 * Use this factory method to create a new instance of this fragment using
	 * the provided parameters.
	 *
	 * @param param1
	 *            Parameter 1.
	 * @param param2
	 *            Parameter 2.
	 * @return A new instance of fragment HomeFragment.
	 */
	// TODO: Rename and change types and number of parameters
	public static HomeFragment newInstance(String param1, String param2) {
		HomeFragment fragment = new HomeFragment();
		Bundle args = new Bundle();
		args.putString(ARG_PARAM1, param1);
		args.putString(ARG_PARAM2, param2);
		fragment.setArguments(args);
		return fragment;
	}
	
	public HomeFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			mParam1 = getArguments().getString(ARG_PARAM1);
			mParam2 = getArguments().getString(ARG_PARAM2);
		}

		if(MainActivity.isNetworkConnected(getActivity()))
		{
			LoadHomeAdpater load= new LoadHomeAdpater();
			load.setListener(this);
			load.execute("home");
		}
		else
		{	
			Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_LONG).show();
		}
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View V= inflater.inflate(R.layout.fragment_home, container, false);

		if(stories_cache!=null)
		{
			ListView list_view =(ListView) V.findViewById(R.id.home_list);
			if(adapter==null)
				adapter =new HomeAdapter(getActivity(),stories_cache);
			list_view.setAdapter(adapter);
		}
		//Log.e("HomeFragment","OnCeateView");
		return V;
	}
	//TODO: This class needs to stand a alone and not be dependent on this fragment. Consider moving to its own ".java"
	class LoadHomeAdpater extends AsyncTask<String,Void,Story[]>
	{
		private OnLoadDataListener mListener;
		private String body,response;
		private HttpURLConnection link;
		private String newTop;
		@Override 
		protected void onPreExecute()
		{
			
		}
		@Override
		protected Story[] doInBackground(String... params) {
			
				URL url;
				try
				{					

					final String BASE_URL= "http://default-environment-fjb3g7bc6t.elasticbeanstalk.com/";
					final String FEED= "feed";
				
					Uri uri= Uri.parse(BASE_URL).buildUpon().appendQueryParameter(FEED,params[0]).build();
					url= new URL(uri.toString());
					link =(HttpURLConnection) url.openConnection();
					setResponse(link.getResponseMessage());
					//link.setReadTimeout(400);
				 	InputStream in = new BufferedInputStream(link.getInputStream());
				 	readStream(in);
				}catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					Log.e(TAG,e.getMessage());
					e.printStackTrace();
					return null;
				}catch (IOException e1) {
					// TODO Auto-generated catch block
					Log.e(TAG,e1.getMessage());
					e1.printStackTrace();
					return null;
				}
			 	
				String json=body;
				Gson gson = new Gson();
				Object jobj = null;
				try {
					jobj = new JSONObject(json).get("feed"); //get the feed Object for JSON string
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Log.e(TAG,e.getMessage());
					return null;
				}
				Feed res=gson.fromJson(jobj.toString(), Feed.class);
				stories_cache = res.getStories();
				newTop = res.getTop();//Update the top of the feed
			return stories_cache;
		}
		public void setListener(OnLoadDataListener listener){
		    mListener = listener;
		}
		protected void readStream(InputStream in)
		  {
			  BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			  StringBuilder sb = new StringBuilder();
			  String line=null;
			  try {
				while ((line=reader.readLine())!= null)
				  {
					  sb.append(line);
				  }
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			  body=sb.toString();
		  }
		@Override
		protected void onPostExecute(Story[] result) {
		    // TODO Auto-generated method stub
			if(result!=null){  
					if (mListener != null && (!top.equals(newTop) && result!=null)) {
						mListener.onLoadComplete(result);
						top=newTop;
					}
			}else
				Toast.makeText(getActivity(), "Connection Error", Toast.LENGTH_LONG).show();
			
		}
		public String getResponse() {
			return response;
		}
		public void setResponse(String response) {
			this.response = response;
		}
	}

	// TODO: Rename method, update argument and hook method into UI event
	public void onButtonPressed(Uri uri) {
		if (mListener != null) {
			mListener.onFragmentInteraction(uri);
		}
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
	}
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (OnFragmentInteractionListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}

	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated to
	 * the activity and potentially other fragments contained in that activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentInteractionListener {
		// TODO: Update argument type and name
		public void onFragmentInteraction(Uri uri);
	}

	@Override
	public void onLoadComplete(Story[] data) {
		// TODO Auto-generated method stub
		try{
			ListView list_view =(ListView) getActivity().findViewById(R.id.home_list);
			if(adapter==null)
				adapter =new HomeAdapter(getActivity(),data);
			list_view.setAdapter(adapter);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
