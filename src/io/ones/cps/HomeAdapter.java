package io.ones.cps;



import java.util.Date;

import com.squareup.picasso.Picasso;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.util.LruCache;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class HomeAdapter extends ArrayAdapter<Story>{

	private LruCache<String,Bitmap> cache;
	private Context mContext;
	private Picasso picasso;
	public HomeAdapter(Context context, Story [] values) {
		super(context,R.layout.row, values);
		mContext=context;
		picasso = Picasso.with(context);
		picasso.setDebugging(true);
		// TODO Auto-generated constructor stub
		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
		
		final int cacheSize = maxMemory / 4;
		cache = new LruCache<String, Bitmap>(cacheSize) {
		      protected int sizeOf(String key, Bitmap bitmap) {
		    	  return bitmap.getByteCount() / 1024;
		  }};
	
		  ;
	}
	
	class ListRow extends LinearLayout{
		private TextView text_view;
		private ImageView image_view;
		private ImageView image_thumb;
		private String url;
		public ListRow(final Context context, AttributeSet attrs) {
			super(context, attrs);
			 LayoutInflater.from(context).inflate(R.layout.row,this);
			 
			 //set the view
			 text_view = (TextView) findViewById(R.id.username);
			 image_view = (ImageView) findViewById(R.id.big_img);
			 image_thumb= (ImageView) findViewById(R.id.profile_thumb);
			 image_view.setScaleType(ImageView.ScaleType.CENTER_CROP);
			 final Intent story=new Intent(mContext,StoryActivity.class);
			 image_view.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					story.putExtra("io.ones.cps.IMAGE_URL", url);
					mContext.startActivity(story);
				}
			 });
		}
		public void setText(String user,String school,long date)
		{
			Date time = new Date(date*1000);
			text_view.setText(user+" "+school+" "+time);
		}
		public void setBigImageDrawable(String id,String url)
		{
			/*image_view.setImageDrawable(getResources().getDrawable(R.drawable.loading));
			if(cache.get(url) != null)
			{
				image_view.setImageBitmap((Bitmap)cache.get(url));
			}
			else
			{

				image_view.setTag(url); //figure out a way to make the initial loading smoother
				
				new LoadImage().execute(image_view,cache);
				image_view.setDrawingCacheEnabled(true);
			}*/
			this.url=url;
			
			picasso.load(url).into(image_view);
		}
		public void setThumbImageDrawable(Drawable D)
		{
			image_thumb.setImageDrawable(D);
		}
		
	}
@Override
 public View getView(int position, View convertView, ViewGroup parent)
 {
	ListRow row;
	if(convertView==null)
	{	
		row = new ListRow(getContext(),null);
		convertView = row;	
	}
	else
	{
		row = (ListRow) convertView;
	}
	Story item = getItem(position);
	row.setText(item.getUserName(),"School Name",item.getDatePosted());
	row.setBigImageDrawable(item.getImageId(),item.getURL());
	return convertView;
 }
}
