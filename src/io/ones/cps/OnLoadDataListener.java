package io.ones.cps;

public interface OnLoadDataListener {
    public void onLoadComplete(Story[] data);
}