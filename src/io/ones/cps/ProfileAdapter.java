package io.ones.cps;

import com.squareup.picasso.Picasso;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class ProfileAdapter extends ArrayAdapter<Story>{

	Context mContext;
	private LruCache<String,Bitmap> cache;
	
	public ProfileAdapter(Context context, Story[] values) {
		super(context, R.layout.cell, values);
		// TODO Auto-generated constructor stub'
		mContext=context;
		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
		
		final int cacheSize = maxMemory / 4;
		cache = new LruCache<String, Bitmap>(cacheSize) {
		      protected int sizeOf(String key, Bitmap bitmap) {
		    	  return bitmap.getByteCount() / 1024;
		       
		  }};
	
	}
	
	class Cell extends FrameLayout
	{
		private ImageView image;
		private Context mContext;
		private String url;
		private Intent story;
		public Cell(Context context) {
			super(context);
			mContext=context;
			LayoutInflater.from(context).inflate(R.layout.cell, this);
			image = (ImageView) findViewById(R.id.small_img);
			story=new Intent(mContext,StoryActivity.class);
			image.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					story.putExtra("io.ones.cps.IMAGE_URL", url);
					Log.v("ProfileAdapter",url);
					mContext.startActivity(story);
					
				}});
			// TODO Auto-generated constructor stub
		}
		
		public void setImage(String url)
		{
			/*image.setImageDrawable(getResources().getDrawable(R.drawable.loading));
			if(cache.get(url)!=null)
			{
				image.setImageBitmap((Bitmap)cache.get(url));
			}
			else
			{
				LoadImage loader =new LoadImage();
				image.setTag(url);
				loader.execute(image,cache);
			}*/
			this.url=url;
			Picasso.with(mContext).load(url).into(image);
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		Cell cell;
		if(convertView==null)
		{
			cell = new Cell(getContext());
			convertView =cell;
		}
		else
		{
			cell = (Cell) convertView;
		}
		Story item=getItem(position);
		cell.setImage(item.getURL());
		return convertView;
	}

}
