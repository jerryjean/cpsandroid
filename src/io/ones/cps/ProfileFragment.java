package io.ones.cps;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

import android.app.Activity;
import android.app.Fragment;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

public class ProfileFragment extends Fragment implements OnLoadDataListener{
	private static final String ARG_PARAM1 = "param1";
	private static final String ARG_PARAM2 = "param2";
	private OnFragmentInteractionListener mListener;
	public final String TAG= ProfileFragment.class.getSimpleName();
	private View rootView;
	public static ProfileFragment newInstance(String param1,String param2)
	{
		ProfileFragment fragment = new ProfileFragment();
		Bundle args = new Bundle();
		args.putString(ARG_PARAM1, param1);
		args.putString(ARG_PARAM2, param2);
		fragment.setArguments(args);
		return fragment;
	}
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		if(MainActivity.isNetworkConnected(getActivity()))
		{
			LoadProfileAdapter load= new LoadProfileAdapter();
			load.setListener(this);
			load.execute("home");
		}
		else
		{	
			Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_LONG).show();
		}
	}
	ProfileFragment()
	{
		//Required empty constructor
	}
	@Override
	public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState)
	{
		rootView = inflater.inflate(R.layout.fragment_profile, container,false);
		ImageView img = (ImageView) rootView.findViewById(R.id.profile_img);
		img.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_camera));
		return rootView;
	}
	public void onButtonPressed(Uri uri) {
		if (mListener != null) {
			mListener.onFragmentInteraction(uri);
		}
	}
	class LoadProfileAdapter extends AsyncTask<String,Void,Story[]>
	{
		private HttpURLConnection link;
		private String body,response;
		private OnLoadDataListener listener;
		@Override
		protected Story[] doInBackground(String... params) {
			// TODO Auto-generated method stub
			URL url;
			try
			{					

				final String BASE_URL= "http://default-environment-fjb3g7bc6t.elasticbeanstalk.com/";
				final String FEED= "feed";
				final String EXTRA="extra";
			
				Uri uri= Uri.parse(BASE_URL).buildUpon().appendQueryParameter(FEED,params[0]).build();
				url= new URL(uri.toString());
				link =(HttpURLConnection) url.openConnection();
				setResponse(link.getResponseMessage());
				//link.setReadTimeout(400);
			 	InputStream in = new BufferedInputStream(link.getInputStream());
			 	readStream(in);
			}catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				Log.e(TAG,e.getMessage());
				e.printStackTrace();
				return null;
			}catch (IOException e1) {
				// TODO Auto-generated catch block
				Log.e(TAG,e1.getMessage());
				e1.printStackTrace();
				return null;
			}
		 	
			String json=body;
			Gson gson = new Gson();
			Object jobj = null;
			try {
				jobj = new JSONObject(json).get("feed"); //get the feed Object for JSON string
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.e(TAG,e.getMessage());
				return null;
			}
			Feed res=gson.fromJson(jobj.toString(), Feed.class);
			return res.getStories();
		}

		public void setListener(OnLoadDataListener nlistener) {
			// TODO Auto-generated method stub
			listener=nlistener;
			
		}
		protected void readStream(InputStream in)
		  {
			  BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			  StringBuilder sb = new StringBuilder();
			  String line=null;
			  try {
				while ((line=reader.readLine())!= null)
				  {
					  sb.append(line);
				  }
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			  body=sb.toString();
		  }
		public String getResponse() {
			return response;
		}
		public void setResponse(String response) {
			this.response = response;
		}
		@Override
		protected void onPostExecute(Story[] data)
		{
			if(data!=null)
				listener.onLoadComplete(data);
		}
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
	}
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (OnFragmentInteractionListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated to
	 * the activity and potentially other fragments contained in that activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentInteractionListener {
		// TODO: Update argument type and name
		public void onFragmentInteraction(Uri uri);
	}

	@Override
	public void onLoadComplete(Story[] data) {
		// TODO Auto-generated method stub
		GridView view = (GridView) getActivity().findViewById(R.id.profile_gridview);
		ProfileAdapter adapter = new ProfileAdapter(getActivity(), data);
		view.setAdapter(adapter);
	}

}
